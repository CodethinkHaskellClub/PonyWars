-- These are safe language extensions we can use to do YAML really easily --
{-# LANGUAGE DeriveGeneric, DeriveAnyClass #-}

module Main where

import GHC.Generics
import Data.Yaml
import qualified Data.ByteString.Char8 as BS

-- We can use the 'deriving' keyword to automatically derive some important Type Classes
-- We derive 'Generic', 'Show' and 'FromJSON' on each of the Color and Pony data types.
-- Show means we can print out the whole data type to the terminal, so if x is a Pony, we
-- can do `print x`. Using Generic and FromJSON together means it will deserialise straight
-- from YAML without any effort!

data Color = Red | Blue | Green deriving (Generic, Show, FromJSON)

data Pony = Pony {
   name :: String,
   numLegs :: Int,
   color :: Color
} deriving (Generic, Show, FromJSON)

-- The fight function takes two ponies and returns the winner who has the most legs.

fight :: Pony -> Pony -> Pony
fight x y = if numLegs x > numLegs y then x else y


-- qsort takes a list of Orderable things, represented by the type signature
-- (Ord (a) => [a] -> [a]). This means the type 'a' must be something that can
-- be compared with a less than and greater than operators '<', '>', '<=', '>='
-- The qsort is implemented with two implementations. The sort of an empty list
-- is just an empty list. The second part of the implementation handles the non-empty
-- case, it breaks the list up into the first element 'x', and the rest of the list 'xs'.
-- This is a common way to talk about functions that take lists.

-- Breaking up the list as x:xs means we can put the [x] in the middle, and break the list
-- up on either side into the ones that are less than x, and the ones that are greater
-- than or equal to x, and qsort on either side recursively.

-- Look up some more list functions at
-- https://hackage.haskell.org/package/base-4.9.0.0/docs/Data-List.html

qsort :: (Ord a) => [a] -> [a]
qsort [] = []
qsort (x:xs) = qsort [y | y <- xs, y < x] ++ [x] ++ qsort [y | y <- xs, y >= x]

main = do
   -- We can load some yaml in as a ByteString, these are in the data/ folder.
   -- bob.yml and smeb.yml each have a single pony in them, where as stables.yml
   -- has a list of ponies, so we can see how to handle each case.
   bobYaml <- BS.readFile "data/bob.yml"
   smebYaml <- BS.readFile "data/smebulok.yml"
   listYaml <- BS.readFile "data/stables.yml"

   -- We can decode the yamls, we have to be explicit about what returns.
   -- The type signature of decode is (FromJSON a) => ByteString -> Maybe a
   -- https://hackage.haskell.org/package/yaml-0.8.0.1/docs/Data-Yaml.html
   -- We will talk more about Maybe next time.
   let x = decode bobYaml :: Maybe Pony
   let y = decode smebYaml :: Maybe Pony
   let ponies = decode listYaml :: Maybe [Pony]

   print x
   print y
   print ponies

   print $ qsort [2,5,3,7,6]
   print $ qsort ["c", "d", "b", "a"]
